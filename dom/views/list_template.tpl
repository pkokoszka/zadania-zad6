<!DOCTYPE html> <!-- The new doctype -->

<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Kolejny blog</title>

    <link rel="stylesheet" type="text/css" href="/~p4/static/styles.css" />

    <!-- Internet Explorer HTML5 enabling script: -->

    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <style type="text/css">

            .clear {
                zoom: 1;
                display: block;
            }

        </style>

    <![endif]-->

</head>

<body>

    <section id="page"> <!-- Defining the #page section with the section tag -->

    <header> <!-- Defining the header section of the page with the appropriate tag -->

        <h1><a href="/~p4/wsgi/">Blog jakiśtam</a></h1>

        <h3>Since 1410</h3>

        <nav class="clear"> <!-- The nav link semantically marks your main site navigation -->

            <ul>
                %if logged:
                    <li><a href="/~p4/wsgi/">Strona główna</a></li>
                    <li><a href="/~p4/wsgi/addpost">Dodaj post</a></li>
                    <li><a href="/~p4/wsgi/logout">Wyloguj się {{username}}</a></li>
                %else:
                    <li><a href="/~p4/wsgi/">Strona główna</a></li>
                    <li><a href="/~p4/wsgi/login">Zaloguj się</a></li>
                %end
            </ul>

        </nav>

    </header>



    <div class="line"></div>  <!-- Dividing line -->

%for item in list:
    <article id="article{{item[0]}}"> <!-- The new article tag. The id is supplied so it can be scrolled into view. -->

        <h2>{{item[1]}}</h2>
        <p>{{item[4]}} {{item[3]}}</p>
        <div class="line"></div>

        <div class="articleBody clear">


            <p>{{item[2]}}</p>


        </div>

    </article>
%end

        <footer> <!-- Marking the footer section -->

            <div class="line"></div>

            <p>PWI</p> <!-- Change the copyright notice -->


        </footer>

    </section> <!-- Closing the #page section -->

    <!-- JavaScript Includes -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script src="jquery.scrollTo-1.4.2/jquery.scrollTo-min.js"></script>
    <script src="/~p4/static/script.js"></script>

    </body>

</html>


