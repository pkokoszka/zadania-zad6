import sqlite3 as lite

con = lite.connect('BookDb.db', check_same_thread=False)
class BookDB():


    #con = lite.connect('BookDb.db')

    with con:

        #cur = con.cursor()
        #cur.execute("CREATE TABLE Books(Id INT, Title TEXT, isbn TEXT, publisher TEXT, author TEXT)")
        #cur.execute("INSERT INTO Books VALUES(1,'CherryPy Essentials: Rapid Python Web Application Development','978-1904811848','Packt Publishing (March 31, 2007)','Sylwia jakastam')")
        #cur.execute("INSERT INTO Books VALUES(2,'Python for Software Design: How to Think Like a Computer Scientist','978-0521725965', 'Cambridge University Press; 1 edition (March 16, 2009)','Sylvain Hellegouarch')")
        #cur.execute("INSERT INTO Books VALUES(3,'Foundations of Python Network Programming','978-1430230038', 'Apress; 2 edition (December 21, 2010)','Allen B. Downey')")
        #cur.execute("INSERT INTO Books VALUES(4,'Python Cookbook, Second Edition','978-1904811848', 'O Reilly Media','Alex Martelli, Anna Ravenscroft, David Ascher')")
        #cur.execute("INSERT INTO Books VALUES(5,'The Pragmatic Programmer: From Journeyman to Master','978-1904811848', 'Addison-Wesley Professional (October 30, 1999)','Andrew Hunt, David Thomas')")

        cur = con.cursor()
        cur.execute("SELECT * FROM Books")

        rows = cur.fetchall()

        for row in rows:
            print row


        # while True:
        #
        #     row = cur.fetchone()
        #
        #     if row == None:
        #         break
        #
        #     print row[0], row[1], row[2]

    def getDb(self):
        cur = con.cursor()
        cur.execute("SELECT * FROM Books")

        rows = cur.fetchall()
        return rows

    def titles(self):

        cur = con.cursor()
        cur.execute("SELECT * FROM Books")

        rows = cur.fetchall()


        titles = [dict(id=row[0], title=row[1]) for row in rows]
        return titles

    def title_info(self, id):

        cur = con.cursor()
        cur.execute("SELECT * FROM Books WHERE Id=:Id", {"Id": id})
        con.commit()

        row = cur.fetchone()
        return row






database = {
    'id1': {'title': 'CherryPy Essentials: Rapid Python Web Application Development',
            'isbn': '978-1904811848',
            'publisher': 'Packt Publishing (March 31, 2007)',
            'author': 'Sylvain Hellegouarch',
            },
    'id2': {'title': 'Python for Software Design: How to Think Like a Computer Scientist',
            'isbn': '978-0521725965',
            'publisher': 'Cambridge University Press; 1 edition (March 16, 2009)',
            'author': 'Allen B. Downey',
            },
    'id3': {'title': 'Foundations of Python Network Programming',
            'isbn': '978-1430230038',
            'publisher': 'Apress; 2 edition (December 21, 2010)',
            'author': 'John Goerzen',
            },
    'id4': {'title': 'Python Cookbook, Second Edition',
            'isbn': '978-0-596-00797-3',
            'publisher': 'O''Reilly Media',
            'author': 'Alex Martelli, Anna Ravenscroft, David Ascher',
            },
    'id5': {'title': 'The Pragmatic Programmer: From Journeyman to Master',
            'isbn': '978-0201616224',
            'publisher': 'Addison-Wesley Professional (October 30, 1999)',
            'author': 'Andrew Hunt, David Thomas',
            },
}
