# -*- coding: utf-8 -*-

import os
import book_app
import unittest
import tempfile
import sqlite3 as lite


class BookAppTestCase(unittest.TestCase):

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.app = book_app.app
        self.app.config['TESTING'] = True
        self.client = self.app.test_client()
        # stworzenie tymczasowego pliku na bazę testową
        # i pobranie uchwytu do pliku oraz jego nazwy
        self.db_fd, self.app.config['DATABASE'] = tempfile.mkstemp()
        # inicjalizacja bazy - dodać kod
        book_app.init_db()

        pass

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        # zamknięcie tymczasowego pliku z bazą testową
        os.close(self.db_fd)
        # skasowanie tymczasowego pliku z bazą testową
        os.unlink(self.app.config['DATABASE'])

    def test_database_setup(self):
        """
        Testuje czy baza została poprawnie zainicjalizowana.
        """
        con = lite.connect('BookDb.db', check_same_thread=False)
        cur = con.cursor()
        # Pobranie informacji o tebeli entries
        cur = con.execute('PRAGMA table_info(Books);')
        rows = cur.fetchall()
        # Tabela entries powinna mieć 3 kolumny
        self.assertEquals(len(rows), 5)
        pass


    def test_get_all_books_empty(self):
        """
        Testuje brak książek w nowej bazie.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            pass

    def test_get_all_books(self):
        """
        Testuje odczyt z bazy wszystkich książek.
        """
        expected = (u"Title", u"isbn")
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()

            # Dodanie nowego wpisu za pomocą dedykowanej funkcji
            microblog.write_entry(*expected)
            # Odczyt wszystkich wpisów za pomocą dedykowanej funkcji
            entries = microblog.get_all_entries()
            # Powinien być tylko jeden wpis
            self.assertEquals(len(entries), 1)
            # Sprawdzenie poprawności odczytu
            for entry in entries:
                self.assertEquals(expected[0], entry['title'])
                self.assertEquals(expected[1], entry['text'])
            pass

    def test_empty_listing(self):
        """
        Testuje odpowiedź strony głównej przy braku książek.
        """
        response = self.client.get('/')
        # dodać kod
        pass

    def test_listing(self):
        """
        Testuje odpowiedź strony głównej ze znanymi książkami.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            pass

    def test_details(self):
        """
        Testuje odpowiedź dla szczegółów konkretnej książki.
        """
        # dodać książki
        response = self.client.get('/id3')
        # dodać kod
        pass

    def test_details_error(self):
        """
        Testuje odpowiedź dla szczegółów błędnej książki.
        """
        # dodać książki
        response = self.client.get('/id33')
        # dodać kod
        pass



if __name__ == '__main__':
    unittest.main()