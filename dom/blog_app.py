# -*- coding: utf-8 -*-

from bottle import route, run, view, get, post, request, response, template, redirect, error, static_file
import blogdb
import bottle
import beaker.middleware
import sqlite3
import datetime
now = datetime.datetime.now()
db = blogdb.BookDB()



session_opts = {
    'session.type': 'file',
    'session.data_dir': './session/',
    'session.cookie_expires': 300,
    'session.auto': True,
}

app = beaker.middleware.SessionMiddleware(bottle.app(), session_opts)


@route('/static/script.js')
def send_static():
    return static_file('script.js', '/~p4/static/')

@route('/static/styles.css')
def send_static():
    return static_file('styles.css', '/~p4/static/')


@route('/')
@view('list_template')
def list():
    posts = db.getPosts()
    return dict(list=posts, logged=loggedInfo()[0], username=loggedInfo()[1])



@get('/logout')
@view('logout_template')
def logout():
    username = request.get_cookie("account", secret=getLoginKey())
    if username:
        response.delete_cookie("account")
        return dict(alert='Wylogowano pomyślnie', logged=loggedInfo()[0], name=loggedInfo()[1])
    else:
        return dict(alert='Nie jesteś zalogowany. Dostęp zabroniony.', logged=loggedInfo()[0], name=loggedInfo()[1])



@get('/login')
@view('login_template')
def login():
    if loggedInfo()[0]:
        redirect("/~p4/wsgi/")
    return dict(logged=False, alert='')

@post('/login')
@view('login_template')
def do_login():
    if loggedInfo()[0]:
        redirect("/~p4/wsgi/")
    username = request.forms.get('username')
    password = request.forms.get('password')
    if check_login(username, password):
        response.set_cookie("account", username, secret=getLoginKey())
        return dict(name=username, logged=True, alert='')
    else:
         return dict(name=username, logged=False, alert='Blad logowania')



@route('/addpost')
@view('addpost_template')
def add_post():
    username = request.get_cookie("account", secret=getLoginKey())
    if username:
        return dict(logged=loggedInfo()[0], username=loggedInfo()[1], alert='')
    else:
        return dict(logged=loggedInfo()[0], username=loggedInfo()[1], alert='')

@post('/addpost')
@view('addpost_template')
def do_addpost():
    if not loggedInfo()[0]:
        redirect("/~p4/wsgi/")

    title = request.forms.get('title')
    content = request.forms.get('content')
    if title is '' or content is '':
        return dict(logged=loggedInfo()[0], username=loggedInfo()[1], alert='Uzupełnij tytuł oraz treść')
    else:
        db.addPost(title.replace("'","''"),content.replace("'","''"),loggedInfo()[1])
        return dict(logged=loggedInfo()[0], username=loggedInfo()[1], alert='Post dodano pomyślnie')



@error(404)
def error404(error):
    return '<h1>404 Not Found<h1/>'


@error(405)
def error404(error):
    return '<h1>405 Method Not Allowed<h1/>'


def loggedInfo():
    username = request.get_cookie("account", secret=getLoginKey())
    if username:
        return True, username
    else:
        return False, ''

def check_login(username, password):
    if username == "admin" and password == "admin1":
        return True
    else:
        return False

def getLoginKey():
    return str(now.year) + str(now.month) + str(now.day)

if __name__ == '__main__':
    #run(host='localhost', port=1784, debug=True)
    bottle.run(app=app, host='localhost', port=1784)