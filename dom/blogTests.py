# -*- coding: utf-8 -*-

import unittest
from blog_app import app
from webtest import TestApp


class blogTests(unittest.TestCase):



    def setUp(self):
        self.app = TestApp(app)


    def tearDown(self):
        self.app.reset()


    def test404(self):
        assert self.app.get('/DASDASDASDASDASSDA', status='*').status_int == 404

    def test405(self):
        assert self.app.post('/', status='*').status_int == 405

    def testMainPage(self):
        self.app.get('/')
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Strona główna' in response
        assert 'Zaloguj się' in response
        assert 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' in response

    def testLogin(self):
        response = self.app.get('/login')
        assert response.status_int == 200
        assert 'Nazwa użytkownika' in response
        assert 'Hasło' in response

    def testLoginWrongUsernameAndPassword(self):
        response = self.app.post('/login', {'username': 'ziom', 'password': 'ziomek'}, status='*')
        assert 'Blad logowania' in response

    def testLoginCorrectUsernameAndPassword(self):
        response = self.app.post('/login', {'username': 'admin', 'password': 'admin1'})
        assert response.status_int == 200
        assert 'Logowanie pomyślne. Za chwilę zostaniesz przekierowny na stronę główną.' in response

    def testForbiddenToAddPostForUnlogged(self):
        response = self.app.get('/addpost', status='*')
        assert 'Dostęp zabroniony. Zostaniesz przekierowany. ' in response


    def testAddpost(self):
        self.app.post('/login', {'username': 'admin', 'password': 'admin1'})
        response = self.app.get('/addpost', status='*')
        assert response.status_int == 200
        assert 'Tytuł postu' in response
        assert 'Treść' in response
        assert 'Formularz dodawania postu' in response

    def testAddPostWrongData(self):
        self.app.post('/login', {'username': 'admin', 'password': 'admin1'})
        response = self.app.post('/addpost', {'title': '', 'content': ''})
        assert 'Uzupełnij tytuł oraz treść' in response

    def testAddPostGoodData(self):
        self.app.post('/login', {'username': 'admin', 'password': 'admin1'})
        response = self.app.post('/addpost', {'title': 'Lorem Ipsum', 'content': 'Deum Deum Lorem Ipsum Ipsum'})
        assert 'Post dodano pomyślnie' in response



if __name__ == '__main__':
    unittest.main()