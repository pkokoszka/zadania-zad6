# -*- coding: utf-8 -*-

from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
import bookdb
import sqlite3

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    database = bookdb.BookDB()

    titles = database.titles()
    return render_template('book_list.html', booksTitles=titles)
    pass


@app.route('/book/<book_id>/')
def book(book_id):
    database = bookdb.BookDB()

    ksiazka = database.title_info(book_id)
    return render_template('book_detail.html', ksiazka=ksiazka)
    pass


if __name__ == '__main__':

    app.run(debug=True)